﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory
{
	class Motorola : IPhone
	{
		public decimal Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
		public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

		public void AccessInternet()
		{
			Console.WriteLine("Welcome to the internet!");
		}

		public void ConnectToDeviceViaNFC(string deviceName)
		{
			Console.WriteLine("No NFC for you");
		}

		public string GetInformation()
		{
			return Environment.NewLine +
			       $"Id :{Id}" + Environment.NewLine +
			       $"Name :{Name}" + Environment.NewLine +
			       "Internet browsing: Yes" + Environment.NewLine +
			       "Bluetooth: No" + Environment.NewLine +
			       "MP3: Yes" + Environment.NewLine +
			       "NFC: No" + Environment.NewLine;
		}

		public void MakeCall(decimal number)
		{
			Console.WriteLine($"Calling {number}");
		}

		public void PlayMP3(string songName)
		{
			Console.WriteLine("Playing " + songName);
		}

		public void PowerOff()
		{
			Console.WriteLine("Device is shooting down!" + Environment.NewLine);
		}

		public void PowerOn()
		{
			Console.WriteLine("Power is on!");
		}

		public void ShareFileOverBluetooth(string fileName)
		{
			throw new NotImplementedException();
		}
	}
}
